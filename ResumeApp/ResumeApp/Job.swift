//
//  Job.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import Foundation

class Job{
    var companyName: String = ""
    var companyLogoName: String = ""
    var position: String = ""
    var description: String = ""
    
    init(_ companyName: String, _ companyLogoName: String, _ position: String, _ description: String) {
        self.companyName = companyName
        self.companyLogoName = companyLogoName
        self.position = position
        self.description = description
    }
}

