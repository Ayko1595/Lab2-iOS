//
//  IdentityTableViewCell.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class IdentityTableViewCell: UITableViewCell {

    @IBOutlet weak var identityImage: UIImageView!
    @IBOutlet weak var identityFirstName: UILabel!
    @IBOutlet weak var identityLastName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        identityImage.layer.cornerRadius = identityImage.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
