//
//  Person.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import Foundation

struct Address {
    let address: String
    let longitude: Double
    let latitude: Double
}

class Person{
    //MARK: Properties
    let firstname: String
    let lastname: String
    let profileImageName: String
    let telephoneNumber: String
    let email: String
    let address: Address
    let website: String
    
    init(firstname: String, lastname: String, profileImageName: String, telephoneNumber: String, email: String, address: Address, website: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.profileImageName = profileImageName
        self.telephoneNumber = telephoneNumber
        self.email = email
        self.address = address
        self.website = website
    }
}
