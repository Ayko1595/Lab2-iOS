//
//  AddressViewController.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import MapKit

class AddressViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var latitude = 0.0
    var longitude = 0.0
    
    var address: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareMapView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareMapView(){
        let span = MKCoordinateSpanMake(0.005, 0.005)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(latitude, longitude), span: span)
        mapView.setRegion(region, animated: true)
        
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = pinLocation
        objectAnnotation.title = address
        self.mapView.addAnnotation(objectAnnotation)
    }

}
