//
//  AnimationViewController.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var animationTableView: UITableView!
    
    var animationType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        switch animationType {
        case "Animation 1":
            animateTable1()
        case "Animation 2":
            animateTable2()
        default:
            animateTable3()
        }
        
    }
    
    func animateTable1() {
        animationTableView.reloadData()
        
        let cells = animationTableView.visibleCells
        let tableHeight: CGFloat = animationTableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    func animateTable2(){
        animationTableView.reloadData()
        let cells = animationTableView.visibleCells
        
        for i in cells{
            let cell: UITableViewCell = i as UITableViewCell
            cell.alpha = 0
        }
        
        var index = 0.0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: index){
                cell.alpha = 1;
            }
            index += 0.2
        }
    }
    
    func animateTable3(){
        animationTableView.reloadData()
        let cells = animationTableView.visibleCells
        
        for i in cells{
            let cell: UITableViewCell = i as UITableViewCell
            cell.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0)
            
        }
        
        var index = 0.0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: index, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            })
            index += 0.2
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AnimationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "animatedCell", for: indexPath) as! StandardTableViewCell
        cell.infoLabel.text = "Row \(indexPath.row)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
}
