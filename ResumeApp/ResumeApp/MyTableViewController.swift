//
//  MyTableViewController.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {
    
    let personKonstantin: Person = Person(firstname: "Konstantin",
                                          lastname: "Ay",
                                          profileImageName: "me_pic",
                                          telephoneNumber: "0722535371",
                                          email: "konstantin.ay95@gmail.com",
                                          address: Address(address: "Plångränd 1, Jönköping", longitude: 14.1502, latitude: 57.7788),
                                          website: "https://www.linkedin.com/in/konstantin-ay-237542b7")
    
    let jobs: [Job] = [Job("Jönköping University",
                           "ju-logo",
                           "Student",
                           "Currently studying Computer Engineering."),
                       Job("Konstantin Ay EF",
                           "abstergo",
                           "Freelance Software Developer",
                           "Working as a freelance Software Developer and under my own company name."),
                       Job("Block21",
                           "block21",
                           "Online Content Manager",
                           "Worked as content manager, i.e. updated lists with correct data for their application Nusic.")]
    
    let animations: [String] = ["Animation 1",
                                "Animation 2",
                                "Animation 3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 5
        case 1:
            return jobs.count
        default:
            return animations.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "identityCell", for: indexPath) as! IdentityTableViewCell
                
               cell.identityFirstName.text = personKonstantin.firstname
               cell.identityLastName.text = personKonstantin.lastname
               cell.identityImage.image = UIImage(named: personKonstantin.profileImageName)
                
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! StandardTableViewCell

                cell.infoLabel.text = personKonstantin.telephoneNumber

                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! StandardTableViewCell

                cell.infoLabel.text = personKonstantin.email

                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! StandardTableViewCell

                cell.infoLabel.text = personKonstantin.address.address

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "websiteCell", for: indexPath) as! StandardTableViewCell

                cell.infoLabel.text = "LinkedIn"

                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "workCell", for: indexPath) as! WorkTableViewCell
            let job = jobs[indexPath.row]
            
            cell.company.text = job.companyName
            cell.companyPhoto.image = UIImage(named: job.companyLogoName)
            cell.companyPosition.text = job.position

            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "animationCell", for: indexPath) as! StandardTableViewCell
            cell.infoLabel.text = animations[indexPath.row]
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return ""
        case 1:
            return "Work Experiences"
        default:
            return "Demos"
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                return 150
            }else{
                return 50
            }
        default:
            return 75
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && (0 < indexPath.row && indexPath.row < 3){
            let cell = tableView.cellForRow(at: indexPath) as! StandardTableViewCell
            let value = cell.infoLabel.text!
            
            if value.contains("@") {
                if let url = URL(string: "mailto:\(value)") {
                    UIApplication.shared.open(url)
                }
            }else{
                UIApplication.shared.open(URL(string: "tel://\(String(describing: value))")!)
            }
            
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddress"{
            let AVC = segue.destination as! AddressViewController
            AVC.address = personKonstantin.address.address
            AVC.longitude = personKonstantin.address.longitude
            AVC.latitude = personKonstantin.address.latitude
        }
        
        if segue.identifier == "showWebsite"{
            let WVC = segue.destination as! WebsiteViewController
            WVC.website = personKonstantin.website
        }
        
        if segue.identifier == "showJob"{
            let JVC = segue.destination as! JobsViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let job = jobs[indexPath.row]
                
                JVC.sendCompanyImageName = job.companyLogoName
                JVC.sendCompanyName = job.companyName
                JVC.sendPositionLabel = job.position
                JVC.sendDescriptionText = job.description
            }
        }
        
        if segue.identifier == "showAnimation"{
            let AVC = segue.destination as! AnimationViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                AVC.animationType = animations[indexPath.row]
            }
        }
    }

}
