//
//  JobsViewController.swift
//  ResumeApp
//
//  Created by Konstantin Ay on 2017-10-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class JobsViewController: UIViewController {

    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    
    var sendCompanyImageName: String = ""
    var sendCompanyName: String = ""
    var sendPositionLabel: String = ""
    var sendDescriptionText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        companyImage.image = UIImage(named: sendCompanyImageName)
        companyNameLabel.text = sendCompanyName
        positionLabel.text = sendPositionLabel
        descriptionText.text = sendDescriptionText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
